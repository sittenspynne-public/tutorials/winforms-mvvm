﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentationModelWinForms
{
    public class VoltageToTemperatureConverter
    {
        public double MinimumVoltage { get; set; }
        public double MaximumVoltage { get; set; }

        public double MinimumTemperature { get; set; }
        public double MaximumTemperature { get; set; }

        public double Convert(double voltageReading)
        {
            double voltageOffset = voltageReading - MinimumVoltage;
            double voltageRange = MaximumVoltage - MinimumVoltage;
            double readingPercent = voltageOffset / voltageRange;

            double temperatureRange = MaximumTemperature - MinimumTemperature;
            double temperature = (readingPercent * temperatureRange) + MinimumTemperature;

            return temperature;
        }

    }
}
