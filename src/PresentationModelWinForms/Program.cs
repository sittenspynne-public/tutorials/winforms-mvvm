﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentationModelWinForms
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(CreateOvenForm());
        }

        private static OvenForm CreateOvenForm()
        {
            return new OvenForm(CreateOvenFormViewModel());
        }

        private static OvenFormViewModel CreateOvenFormViewModel()
        {
            return new OvenFormViewModel(CreateTemperatureReader());
        }

        private static TemperatureReader CreateTemperatureReader()
        {
            return new TemperatureReader(
                CreateOven(),
                CreateVoltageToTemperatureConverter());
        }

        private static Oven CreateOven()
        {
            return new Oven();
        }

        private static VoltageToTemperatureConverter CreateVoltageToTemperatureConverter()
        {
            return new VoltageToTemperatureConverter()
            {
                MinimumVoltage = 0.4,
                MaximumVoltage = 2.8,
                MinimumTemperature = 100,
                MaximumTemperature = 550
            };
        }
    }
}
