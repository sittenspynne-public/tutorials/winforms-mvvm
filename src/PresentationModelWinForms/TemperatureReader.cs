﻿using System;
using System.Threading;

namespace PresentationModelWinForms
{
    public class TemperatureReader
    {
        private static readonly TimeSpan UpdateInterval = TimeSpan.FromSeconds(1);

        private readonly Oven _oven;
        private readonly VoltageToTemperatureConverter _converter;

        private readonly Timer _timer;

        public TemperatureReader(Oven oven, VoltageToTemperatureConverter converter)
        {
            _converter = converter;
            _oven = oven;

            _timer = new Timer(WhenTimerTicks, null, UpdateInterval, UpdateInterval);
        }

        private void WhenTimerTicks(object _)
        {
            var voltage = _oven.ReadThermistor();
            var temperature = _converter.Convert(voltage);

            OnReadingMade(temperature);
        }

        protected void OnReadingMade(double reading)
        {
            ReadingMade?.Invoke(this, new TemperatureReadingEventArgs(reading));
        }

        public event EventHandler<TemperatureReadingEventArgs> ReadingMade;
    }
}