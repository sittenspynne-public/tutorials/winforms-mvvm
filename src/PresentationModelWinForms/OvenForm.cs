﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentationModelWinForms
{
    public partial class OvenForm : Form
    {
        private readonly OvenFormViewModel _viewModel;

        public OvenForm(OvenFormViewModel viewModel)
        {
            _viewModel = viewModel;
            InitializeComponent();

            viewModel.PropertyChanged += WhenAViewModelPropertyChanges;
        }

        private void WhenAViewModelPropertyChanges(object sender, PropertyChangedEventArgs e)
        {
            this.Invoke(new Action(() => SafeWhenAViewModelPropertyChanges(e)));
        }

        private void SafeWhenAViewModelPropertyChanges(PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OvenFormViewModel.Temperature))
            {
                textBox1.Text = _viewModel.Temperature.ToString("F0");
            }
        }
    }
}
