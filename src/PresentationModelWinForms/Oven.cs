﻿using System;

namespace PresentationModelWinForms
{
    public class Oven
    {
        private readonly Random _rng = new Random();
        public double ReadThermistor()
        {
            // Return a random double between 0.4 and 2.8.
            return (_rng.NextDouble() * 2.4) + 0.4;
        }
    }
}