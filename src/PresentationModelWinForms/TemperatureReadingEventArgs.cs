﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentationModelWinForms
{
    public class TemperatureReadingEventArgs : EventArgs
    {
        public double Temperature { get; set; }

        public TemperatureReadingEventArgs(double temperature)
        {
            Temperature = temperature;
        }
    }
}
