﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace PresentationModelWinForms
{
    public class OvenFormViewModel : INotifyPropertyChanged
    {
        private readonly TemperatureReader _reader;

        private double _temperature;

        public double Temperature
        {
            get { return _temperature; }
            set
            {
                if (_temperature != value)
                {
                    _temperature = value;
                    OnPropertyChanged();
                }
            }
        }

        public OvenFormViewModel(TemperatureReader reader)
        {
            _reader = reader;

            _reader.ReadingMade += WhenAReadingIsMade;
        }

        private void WhenAReadingIsMade(object sender, TemperatureReadingEventArgs e)
        {
            Temperature = e.Temperature;
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}