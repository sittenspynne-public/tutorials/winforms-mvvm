# What is this?

This is a progressive tutorial attempting to demonstrate something like MVVM in terms of a WinForms project.

## Why?

It's an interesting topic to explain, and I don't think a lot of people talk about it in terms of WinForms. Seeing how to approximate something like WPF's data binding yourself is very useful. Implmenting your own IoC container helps you understand why it exists.

## What do I need to know?

I assume the reader is familiar enough with Windows Forms to write single-form applications. 

## What is the project going to do?

In an analogy, I compared Presentation Model patterns to like the relationship between an oven, its tempearature sensors, and its display. The application is going to stick with that analogy.

There is going to be something that represents a thermistor. It's job is to take and report a temperature reading. That temperature will be in terms of a voltage, so something will have to convert it to a temperature.

There is going to be a form that represents the oven display. It will display the most current temperature reading.

There is going to be something that periodically asks for the current temperature and updates it. 

The analogy discussed a seven-segment display and some other complexities. I don't plan on the project getting that complicated. I'm here to demonstrate separation of concerns and Presentation Model thoughts, not demonstrate custom WinForms controls!

# Contents

| Tag | Description|
| --- | --- |
| v1 | Messy initial implementation to get a feel for how everything is put together. |
